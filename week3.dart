import 'dart:convert';

String jsonRaw = '{ "id": 1, "url": "https://via.placeholder.com/600/92c952&quot"}';

class ImageModel {
  int? id;
  String? url;

ImageModel(this.id, this.url);

ImageModel.fromJson(Map<String, dynamic> jsonObject) {
  id = jsonObject['id'];
  url = jsonObject['url'];
}

String toString() {
  return '($id, $url)';
  }
}

void main() {
  print(jsonRaw);
  var jsonObject = json.decode(jsonRaw);

  print(jsonObject['id']);


  var image1 = ImageModel(jsonObject['id'], jsonObject['url']);
  print(image1.url);
  print(image1.id);
}

